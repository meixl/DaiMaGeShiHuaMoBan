# 代码格式化模版

#### 介绍

Java 代码格式化，Idea Eclipse 均有

#### idea 使用注意使用

idea 安装 Eclipse Formater 插件，选择`CodeConvention_Eclipse.xml`导入即可。

idea 自己格式化，链式调用很蠢，在 review 时并不关注链式的每一步。链式太长，也可以断开写成多个。

`idea`->`Code Style`->`Java` 使用默认即可。

~~* idea 导入格式化模版，请先禁用 idea 中的 Eclipse 格式化插件。该插件会优先使用 Eclipse 的格式化文件。~~

#### 自动格式化

1. idea 安装 save action 插件

2. 设置保存时长。建议 5 秒以上，时间过短则在联想时，可能因为来不及选择而格式化，这时候就要重新联想打单词了。

   ![我是一个图片](/pic/微信图片_20190614165450.png)

    我是看的这个文章，[点我传送](https://blog.csdn.net/wangjun5159/article/details/55223630)。

#### vue 空 2 格

1. 首先`.eslintrc.js`需要应用格式化，右键->Apply ESlint Code Style Rules

2. Settings->Editor->Code Style->Html: Do not indent children of 添加 script

 ![我是一个图片](/pic/ccf332d97abf0649a732850726474b8.png)

#### 目前的不足

0. 使用`CodeConvention_Eclipse.xml`暂时未发现不足。不足是`CodeConvention_Idea.xml`文件。

1. 无法做到 Eclipse 将 pojo 中属性排列整齐。Eclipse 有如下设置，idea 中暂时没有找到对应选项。

 ![我是一个图片](/pic/微信图片_20190617142211.png)

2. pojo 中 serialVersionUID 与第一个字段的注释，请空一行。不空行，就像下图一样，代码不美观，暂未找到解决办法。

 ![我是一个图片](/pic/微信图片_20190617143439.png)

   idea pojo 生成 serialVersionUID 方法见下图。

 ![我是一个图片](/pic/微信图片_20190620090755.png)

![输入图片说明](pic/2e3b0805097e29b96f9ba981d26f532.png)

3. @Autowired 黄线

![输入图片说明](pic/e03b3c6d25f9b2dee977f870126ca22.png)